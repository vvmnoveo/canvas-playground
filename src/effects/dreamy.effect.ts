export const applyDreamy = (context: CanvasRenderingContext2D, width: number, height: number) => {
  const blurRadius = 2;
  context.globalAlpha = 0.5;

  for (let y = -blurRadius; y <= blurRadius; y += 1) {
    for (let x = -blurRadius; x <= blurRadius; x += 1) {
      context.drawImage(context.canvas, x, y);
    }
  }

  context.globalAlpha = 1;
};