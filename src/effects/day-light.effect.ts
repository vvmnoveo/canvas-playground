export const applyDayLight = (context: CanvasRenderingContext2D, width: number, height: number) => {
  const saturation = 0.5;
  const imageData = context.getImageData(0, 0, width, height);
  const data = imageData.data;

  for (let i = 0; i < data.length; i += 4) {
    const r = data[i];
    const g = data[i + 1];
    const b = data[i + 2];

    const gray = 0.2989 * r + 0.5870 * g + 0.1140 * b;

    data[i] = r + (gray - r) * saturation;
    data[i + 1] = g + (gray - g) * saturation;
    data[i + 2] = b + (gray - b) * saturation;
  }

  context.putImageData(imageData, 0, 0);
};