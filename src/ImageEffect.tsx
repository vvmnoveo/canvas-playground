import React, { useRef, useEffect } from 'react';
import {applyBlackAndWhite, applyDayLight, applyDreamy, applySepia, applySunLight, applyVintage} from "./effects";

export type Effect =
  | 'black & white'
  | 'sepia'
  | 'vintage'
  | 'sun light'
  | 'dreamy'
  | 'day light';

interface ImageEffectProps {
  src: string;
  effect: Effect;
}

export const ImageEffect: React.FC<ImageEffectProps> = ({ src, effect }) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    if (canvasRef.current) {
      const canvas = canvasRef.current;
      const context = canvas.getContext('2d');
      const img = new Image();
      img.src = src;

      img.onload = () => {
        canvas.width = img.width;
        canvas.height = img.height;
        if (!context) return;
        context.drawImage(img, 0, 0, img.width, img.height);



        // Apply effects here
        switch (effect) {
          case 'black & white':
            applyBlackAndWhite(context, img.width, img.height)
            break;
          case 'sepia':
            applySepia(context, img.width, img.height);
            break;
          case 'vintage':
            applyVintage(context, img.width, img.height);
            break;
          case 'sun light':
            applySunLight(context, img.width, img.height);
            break;
          case 'dreamy':
            applyDreamy(context, img.width, img.height);
            break;
          case 'day light':
            applyDayLight(context, img.width, img.height);
            break;
        }
      };
    }
  }, [canvasRef, src, effect]);

  return <canvas  width={320} ref={canvasRef}></canvas>;
};
