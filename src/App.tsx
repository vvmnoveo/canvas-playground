import React, { useState } from 'react';
import { ImageEffect, Effect } from './ImageEffect';
import './App.css';

const App: React.FC = () => {
  const [effect, setEffect] = useState<Effect>('black & white');
  // random image from https://picsum.photos/
  const imageSrc = '/example.jpg';

  return (
    <div className="App">
      <h1>Image Effects</h1>
      <select value={effect} onChange={(e) => setEffect(e.target.value as Effect)}>
        <option value="black & white">Black & White</option>
        <option value="sepia">Sepia</option>
        <option value="vintage">Vintage</option>
        <option value="sun light">Sun Light</option>
        <option value="dreamy">Dreamy</option>
        <option value="day light">Day Light</option>
      </select>
      <div style={{ display: 'flex', flexDirection: 'row',
        // in center
        justifyContent: 'center',
      }}>
        <div>
          <h2>Original</h2>
          <img src={imageSrc} alt="Original" />
        </div>
        <div>
          <h2>With Effect</h2>
          <ImageEffect src={imageSrc} effect={effect} />
        </div>
      </div>
    </div>
  );
};

export default App;
