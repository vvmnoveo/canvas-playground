export const applySunLight = (context: CanvasRenderingContext2D, width: number, height: number) => {
  const brightness = 0.3;
  const imageData = context.getImageData(0, 0, width, height);
  const data = imageData.data;

  for (let i = 0; i < data.length; i += 4) {
    data[i] += 255 * brightness;
    data[i + 1] += 255 * brightness;
    data[i + 2] += 255 * brightness;
  }

  context.putImageData(imageData, 0, 0);
};
